require(['jquery','Magento_Ui/js/modal/modal','domReady!'],
    function ($,modal) {
        $('.product-inform_original').click(function(){
            $('.product-original_popup').toggleClass('product-info_active');
        });

        $('.product-inform_delivery').click(function(){
            $('.product-delivery_popup').toggleClass('product-info_active');
        });

        $('.delivery-terms').click(function(){
            $('.delivery-terms_inform').toggleClass('product-info_active');
        });

        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: true,
            clickableOverlay: true,
            buttons: []
        };

        var popup = modal(options, $('.product-question_popup'));
        $(".product-inform_question").on('click',function(){
            $(".product-question_popup").modal("openModal");
        });

    });

