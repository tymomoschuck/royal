var config = {
paths: {
	slick:   'js/vendor/slick/slick',
	product: 'js/product/product'
},
shim: {
	slick: {
		deps: ['jquery']
	}
},
	deps: [
		'js/all',
		'js/product/product'
	]
};
